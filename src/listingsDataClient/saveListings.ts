import { PrismaClient } from "@prisma/client";
import { RentListing } from "./types/listings";

const prisma = new PrismaClient();

/**
 * Given a RentListing object and a platformId, saves the given listing
 * to the database and returns the unique id of the saved listing if the
 * insert was successful.
 */
export const saveListing = async (
  listing: RentListing,
  platformId: number
): Promise<string> => {
  const dbListing = await prisma.listing.create({
    data: {
      platformId,
      platformUserId: listing.ownerId,
      monthlyRent: listing.rentPrice,
      city: listing.location.city,
      country: listing.location.country,
      address: JSON.stringify(listing.location.address),
      houseDetails: {
        numOfRooms: listing.houseDetails.numOfRooms,
        numOfBedrooms: listing.houseDetails.numOfBedrooms,
        numOfBathrooms: listing.houseDetails.numOfBathrooms,
        wallMaterial: listing.houseDetails.wallMaterial,
        floorMaterial: listing.houseDetails.floorMaterial,
      },
      imageUrls: listing.imageUrls,
    },
  });
  return dbListing.id;
};
