/*
  Warnings:

  - You are about to drop the column `price` on the `PartialSearch` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "PartialSearch" DROP COLUMN "price",
ADD COLUMN     "monthlyRent" INTEGER;
