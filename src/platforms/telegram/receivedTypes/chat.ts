import { Message } from "./message";

export type Chat = {
  id: number;
  type: "private" | "group" | "supergroup" | "channel";
  username?: string;
  first_name?: string;
  last_name?: string;
  // Skipping a bunch of fields
  photo?: ChatPhoto;
  pinned_message?: Message;
};

export type ChatPhoto = {
  small_file_id: string;
  small_file_unique_id: string;
  big_file_id: string;
  big_file_unique_id: string;
};
