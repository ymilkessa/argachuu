export type ChatMessageComponents<PlatformMessage> = {
  engagementIdentifier: string | number;
  userIdentifier: string | number;
  message: PlatformMessage;
};

export abstract class ChatMessageWrapper<PlatformMessage> {
  engagementIdentifier: string | number;
  userIdentifier: string | number;
  message: PlatformMessage;

  constructor({
    engagementIdentifier,
    userIdentifier,
    message,
  }: ChatMessageComponents<PlatformMessage>) {
    this.engagementIdentifier = engagementIdentifier;
    this.userIdentifier = userIdentifier;
    this.message = message;
  }

  /**
   * Returns the text of the message.
   * Returns undefined if the message does not contain text.
   */
  public abstract getMessageText(): string | undefined;
}
