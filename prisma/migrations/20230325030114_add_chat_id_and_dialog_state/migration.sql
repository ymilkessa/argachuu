/*
  Warnings:

  - A unique constraint covering the columns `[chatId]` on the table `PartialListing` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[chatId]` on the table `PartialSearch` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `chatId` to the `PartialListing` table without a default value. This is not possible if the table is not empty.
  - Added the required column `currentState` to the `PartialListing` table without a default value. This is not possible if the table is not empty.
  - Added the required column `chatId` to the `PartialSearch` table without a default value. This is not possible if the table is not empty.
  - Added the required column `currentState` to the `PartialSearch` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "PartialListing" ADD COLUMN     "chatId" BIGINT NOT NULL,
ADD COLUMN     "currentState" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "PartialSearch" ADD COLUMN     "chatId" BIGINT NOT NULL,
ADD COLUMN     "currentState" INTEGER NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "PartialListing_chatId_key" ON "PartialListing"("chatId");

-- CreateIndex
CREATE UNIQUE INDEX "PartialSearch_chatId_key" ON "PartialSearch"("chatId");
