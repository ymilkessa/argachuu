-- CreateTable
CREATE TABLE "TgContactInfo" (
    "platformUserId" TEXT NOT NULL,
    "platformUserName" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "TgContactInfo_platformUserId_key" ON "TgContactInfo"("platformUserId");
