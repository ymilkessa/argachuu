// T stands for tenant
// LL stands for landlord
export enum DIALOG_STATE {
  /**
   * Return the opening message.
   */
  OPENING = 0,
  /**
   * This is where the bot asks if the user wants to engage as a landlord or tenant.
   */
  START_DIALOG = 100,
  /**
   * (If the user is a landlord and has posted listings recently, first
   * state the number of listings they have posted.)
   * Ask the user to post pictures of a new listing.
   */
  LL_1_PICTURES = 1,
  /**
   * Prompt the user to either send more pictures, or to send '.' if they
   * are done sending pictures.
   */
  LL_2_MORE_PICTURES = 2,
  /**
   * Ask the user for the number of rooms in the house.
   */
  LL_3_NUM_OF_ROOMS = 3,
  /**
   * Ask the user to enter the district that the house is located in.
   */
  LL_4_DISTRICT = 4,
  /**
   * Ask the user to enter the monthly rent.
   */
  LL_5_MONTHLY_RENT = 5,
  /**
   * Ask the user which listing they want to delete.
   */
  LL_6_INITIATE_DELETE_LISTING = 6,
  /**
   * Delete the listing whose number the user entered.
   */
  LL_7_DELETE_LISTING = 7,
  /**
   * Ask the user for the district they are interested in.
   */
  T_1_DISTRICT = 101,
  /**
   * Ask the user for the maximum rent they are willing to pay.
   */
  T_2_MAX_RENT = 102,
  /**
   * In this state, you give the user the option of starting again by enetering #,
   * or getting information on any of the given listings by entering the listing
   * number.
   */
  T_3_RESULTS = 103,
}
