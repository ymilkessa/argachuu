import { ImageDisplay } from "./imageDisplay";
import { PlainText } from "./plainText";

export enum MessageType {
  TEXT = "text",
  IMAGES = "images",
}

export type GenericMessage =
  | {
      type: MessageType.TEXT;
      payload: PlainText;
    }
  | {
      type: MessageType.IMAGES;
      payload: ImageDisplay;
    };
