import { QueryHub, USER_TYPES_ENUM } from "../languages/english/queryHub";
import { DIALOG_STATE } from "./dialogStates";
import {
  Listing,
  PartialListing,
  PartialSearch,
  PrismaClient,
  TgEngagementRecord,
} from "@prisma/client";
import { PLATFORM_IDS } from "../platforms/index/platforms";
import {
  ADDIS_DISTRICTS,
  ADDIS_DISTRICT_CODES,
} from "../languages/english/addressOptions";
import { HouseDetails } from "../listingsDataClient/types/listings";
import { BotManager } from "../platforms/telegram/botManager";
import { GenericMessage, MessageType } from "./primitives/genericMessage";
import { ChatMessageWrapper } from "./chatMessage";
import { PlainText } from "./primitives/plainText";

const prismaClient = new PrismaClient();

/**
 * This class can determine the state of the dialog with a user and return the next
 * message to send. It should be instantiated for each new message from a user.
 */
export class DialogHandler<PlatformMessageType> {
  private queryHub: QueryHub;
  private dialogState: DIALOG_STATE;
  private inputMessage: ChatMessageWrapper<PlatformMessageType>;
  private botManager: BotManager;
  private startOverSymbol = "*";
  private deleteListingSymbol = "!";
  private numberOfListings: number = 0;

  private constructor(
    queryHub: QueryHub,
    dialogState: DIALOG_STATE,
    inputMessage: ChatMessageWrapper<PlatformMessageType>,
    botManager: BotManager
  ) {
    this.queryHub = queryHub;
    this.dialogState = dialogState;
    this.inputMessage = inputMessage;
    // this.storageClient = new StorageClient();
    this.botManager = botManager;
  }

  /**
   * Creates a new DialogHandler instance after identifying the state of the dialog with the user
   * based on the given message (and database record).
   */
  public static async getDialogHandler<PlatformMessageType>(
    message: ChatMessageWrapper<PlatformMessageType>,
    queryHub: QueryHub,
    botManager: BotManager
  ): Promise<DialogHandler<PlatformMessageType>> {
    if (!message.engagementIdentifier) {
      throw new Error("Telegram chat ID not found.");
    }
    const dialogState = await this.getDialogState(message);
    return new DialogHandler<PlatformMessageType>(
      queryHub,
      dialogState,
      message,
      botManager
    );
  }

  public async getNextMessages(): Promise<GenericMessage[]> {
    const strippedMessage = this.inputMessage.getMessageText();

    let pictureUrls: string[] = [];
    let district: string | undefined = undefined;
    let districtCode: number | undefined = undefined;
    let monthlyRent: number | undefined = undefined;
    let partialListingEntry: PartialListing | null = null;
    let partialSearchEntry: PartialSearch | null = null;
    /**
     * The next state if this exchange is successful.
     */
    let nextState = this.dialogState;
    /**
     * Array of messages to return to the user.
     */
    const responses: GenericMessage[] = [];

    // Load the engagement record and partial listing/search entries if they exist.
    const engagementRecord = await prismaClient.tgEngagementRecord.findUnique({
      where: {
        chatId: Number(this.inputMessage.engagementIdentifier),
      },
    });
    if (engagementRecord) {
      if (engagementRecord.partialListingId) {
        partialListingEntry = await prismaClient.partialListing.findUnique({
          where: {
            id: engagementRecord.partialListingId,
          },
        });
      }
      if (engagementRecord.partialSearchId) {
        partialSearchEntry = await prismaClient.partialSearch.findUnique({
          where: {
            id: engagementRecord.partialSearchId,
          },
        });
      }
    }

    // Check if the user has any listings.
    const numListings = await prismaClient.listing.count({
      where: {
        platformId: PLATFORM_IDS.TELEGRAM,
        platformUserId: String(this.inputMessage.userIdentifier),
      },
    });
    this.numberOfListings = numListings;

    // Handle the "start over" request.
    if (strippedMessage === this.startOverSymbol) {
      this.dialogState = DIALOG_STATE.OPENING;
    }
    // TODO: secret code to start over, with a completely clean slate.
    if (
      engagementRecord &&
      strippedMessage === "865a0137-8559-463b-bf82-654bb0b1ee2a"
    ) {
      const newRecordEntry = await this.clearEngagementRecord(engagementRecord);
      await prismaClient.tgEngagementRecord.delete({
        where: {
          id: newRecordEntry.id,
        },
      });
      this.dialogState = DIALOG_STATE.OPENING;
      return [];
    }

    // Handle the "delete listing" request.
    if (strippedMessage === this.deleteListingSymbol) {
      // Clear current engagement because it was interrupted
      if (engagementRecord) {
        await this.clearEngagementRecord(engagementRecord);
      }
      if (!this.numberOfListings) {
        responses.push(
          this.textMessage(
            this.queryHub.ADD_LISTING_NOTICES.NO_LISTINGS_TO_DELETE
          )
        );
        this.dialogState = DIALOG_STATE.OPENING;
      } else {
        this.dialogState = DIALOG_STATE.LL_6_INITIATE_DELETE_LISTING;
      }
    }

    // If this is not the opening state, and there is no engagement record, throw an error.
    if (this.dialogState !== DIALOG_STATE.OPENING && !engagementRecord) {
      throw new Error(
        "Engagement record not found for chat ID: " +
          this.inputMessage.engagementIdentifier
      );
    }

    switch (this.dialogState) {
      case DIALOG_STATE.OPENING:
        nextState = DIALOG_STATE.START_DIALOG;
        if (engagementRecord) {
          await this.clearEngagementRecord(engagementRecord);
          await prismaClient.tgEngagementRecord.update({
            where: {
              chatId: Number(this.inputMessage.engagementIdentifier),
            },
            data: {
              currentState: nextState,
            },
          });
        } else {
          await prismaClient.tgEngagementRecord.create({
            data: {
              platformUserId: String(this.inputMessage.userIdentifier),
              chatId: Number(this.inputMessage.engagementIdentifier),
              currentState: nextState,
              lastEngagementType: USER_TYPES_ENUM.UNIDENTIFIED,
            },
          });
          responses.push(this.textMessage(this.introStatement()));
        }
        responses.push(this.textMessage(this.promptForUserType()));
        break;

      case DIALOG_STATE.START_DIALOG:
        // If the stripped message is a number, then get the user type enum that it corresponds to.
        const userType = Number(strippedMessage);
        if (userType === USER_TYPES_ENUM.LANDLORD) {
          nextState = DIALOG_STATE.LL_1_PICTURES;
          // First get how many listings this landlord has.
          const numListings = await prismaClient.listing.count({
            where: {
              platformId: PLATFORM_IDS.TELEGRAM,
              platformUserId: String(this.inputMessage.userIdentifier),
            },
          });
          if (numListings > 0) {
            responses.push(
              this.textMessage(
                this.queryHub.ADD_LISTING_NOTICES.STATE_NUM_OF_LISTINGS +
                  ` ${numListings}`
              )
            );
          }
          responses.push(
            this.textMessage(this.queryHub.ADD_LISTING_QUERIES.PICTURES)
          );
          // Set the lastEngagementType to landlord, and the currentState to LL_1_PICTURES.
          await prismaClient.tgEngagementRecord.update({
            where: {
              chatId: Number(this.inputMessage.engagementIdentifier),
            },
            data: {
              lastEngagementType: USER_TYPES_ENUM.LANDLORD,
              currentState: nextState,
            },
          });
        } else if (userType === USER_TYPES_ENUM.TENANT) {
          nextState = DIALOG_STATE.T_1_DISTRICT;
          responses.push(
            this.textMessage(this.districtsMenuWithPrompt(nextState))
          );
          await prismaClient.tgEngagementRecord.update({
            where: {
              chatId: Number(this.inputMessage.engagementIdentifier),
            },
            data: {
              lastEngagementType: USER_TYPES_ENUM.TENANT,
              currentState: nextState,
            },
          });
        } else {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
        }
        break;

      case DIALOG_STATE.LL_1_PICTURES:
        nextState = DIALOG_STATE.LL_2_MORE_PICTURES;
        pictureUrls = [];
        try {
          pictureUrls =
            await this.botManager.fetchAndSavePhotos<PlatformMessageType>(
              this.inputMessage
            );
        } finally {
          // If no pictures are provided, or if there was an error processing the last message,
          // then send an error message and prompt again.
          if (pictureUrls.length === 0) {
            responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
            break;
          }
        }

        if (!partialListingEntry) {
          // Create a new partial listing entry.
          const newPartialListing = await prismaClient.partialListing.create({
            data: {
              imageUrls: pictureUrls,
            },
          });
          // Update the engagement record to point to the new partial listing.
          await prismaClient.tgEngagementRecord.update({
            where: {
              chatId: Number(this.inputMessage.engagementIdentifier),
            },
            data: {
              partialListingId: newPartialListing.id,
            },
          });
        } else if (partialListingEntry) {
          // Update the partial listing entry to add the new pictures.
          await prismaClient.partialListing.update({
            where: {
              id: partialListingEntry.id,
            },
            data: {
              imageUrls: {
                push: pictureUrls,
              },
            },
          });
        }
        responses.push(
          this.textMessage(this.queryHub.ADD_LISTING_QUERIES.MORE_PICTURES)
        );
        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
          },
        });
        break;

      case DIALOG_STATE.LL_2_MORE_PICTURES:
        nextState = DIALOG_STATE.LL_3_NUM_OF_ROOMS;
        if (!engagementRecord || !engagementRecord.partialListingId) {
          throw new Error("Partial listing entry not.");
        }
        // If the trimmed message text is just '#', then we are done with pictures.
        if (strippedMessage === "#") {
          // Just move on to the next step by updating the status on the engagement record.
          await prismaClient.tgEngagementRecord.update({
            where: {
              chatId: Number(this.inputMessage.engagementIdentifier),
            },
            data: {
              currentState: nextState,
            },
          });
          responses.push(
            this.textMessage(this.queryHub.ADD_LISTING_QUERIES.NUM_OF_ROOMS)
          );
          break;
        }
        pictureUrls =
          await this.botManager.fetchAndSavePhotos<PlatformMessageType>(
            this.inputMessage
          );
        if (pictureUrls.length === 0) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        await prismaClient.partialListing.update({
          where: {
            id: engagementRecord.partialListingId,
          },
          data: {
            imageUrls: {
              push: pictureUrls,
            },
          },
        });
        responses.push(
          this.textMessage(this.queryHub.ADD_LISTING_QUERIES.MORE_PICTURES)
        );
        break;

      case DIALOG_STATE.LL_3_NUM_OF_ROOMS:
        nextState = DIALOG_STATE.LL_4_DISTRICT;
        if (!engagementRecord || !engagementRecord.partialListingId) {
          throw new Error("Engagement record not found.");
        }
        const numOfRooms = Number(strippedMessage);
        if (isNaN(numOfRooms)) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        await prismaClient.partialListing.update({
          where: {
            id: engagementRecord.partialListingId,
          },
          data: {
            numberOfRooms: numOfRooms,
          },
        });
        responses.push(
          this.textMessage(this.districtsMenuWithPrompt(nextState))
        );
        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
          },
        });
        break;

      case DIALOG_STATE.LL_4_DISTRICT:
        nextState = DIALOG_STATE.LL_5_MONTHLY_RENT;
        if (!engagementRecord || !engagementRecord.partialListingId) {
          throw new Error("Engagement record not found.");
        }
        // Check if the message is a valid district code (one of the numeric values in the ADDIS_DISTRICT_CODES object)
        districtCode = Number(strippedMessage);
        // get the district key whose value is the district code
        district = Object.keys(ADDIS_DISTRICT_CODES).find(
          (key: any) =>
            ADDIS_DISTRICT_CODES[key as keyof typeof ADDIS_DISTRICT_CODES] ===
            districtCode
        );
        if (isNaN(districtCode) || !district) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        await prismaClient.partialListing.update({
          where: {
            id: engagementRecord.partialListingId,
          },
          data: {
            district,
          },
        });
        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
          },
        });
        responses.push(
          this.textMessage(this.queryHub.ADD_LISTING_QUERIES.MONTHLY_RENT)
        );
        break;

      case DIALOG_STATE.LL_5_MONTHLY_RENT:
        nextState = DIALOG_STATE.OPENING;
        if (!engagementRecord || !partialListingEntry) {
          throw new Error("Engagement record not found.");
        }
        monthlyRent = Number(strippedMessage);
        if (isNaN(monthlyRent)) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        // First ensure that the fields of the partial listing are non-null
        if (
          !partialListingEntry.imageUrls ||
          !partialListingEntry.numberOfRooms ||
          !partialListingEntry.district
        ) {
          throw new Error("Partial listing is missing required fields.");
        }
        // Convert the rent to a whole number
        monthlyRent = Math.round(monthlyRent);
        await prismaClient.partialListing.update({
          where: {
            id: partialListingEntry.id,
          },
          data: {
            monthlyRent,
          },
        });
        const saveUserName = this.botManager.saveUsername(
          this.inputMessage.message
        );
        if (!saveUserName) {
          responses.push(
            this.textMessage(
              this.queryHub.ADD_LISTING_NOTICES.USERNAME_NOT_IDENTIFIED
            )
          );
          await this.clearEngagementRecord(engagementRecord);
          break;
        }
        // Create the listing using data from the partial listing and the provided monthly rent
        const listing = await prismaClient.listing.create({
          data: {
            platformId: PLATFORM_IDS.TELEGRAM,
            platformUserId: String(this.inputMessage.userIdentifier),
            monthlyRent,
            imageUrls: partialListingEntry.imageUrls,
            houseDetails: {
              numOfRooms: partialListingEntry.numberOfRooms,
            } as HouseDetails,
            address: partialListingEntry.district,
            city: "Addis Ababa",
            country: "Ethiopia",
          },
        });

        if (!listing) {
          throw new Error("Listing could not be created.");
        }
        // Mark that this user has listings
        this.numberOfListings += 1;
        responses.push(
          this.textMessage(
            this.queryHub.ADD_LISTING_NOTICES.POST_ADD_LISTING_MESSAGE
          )
        );
        // Clear engagement record (including partial listing)
        await this.clearEngagementRecord(engagementRecord, false);
        break;

      case DIALOG_STATE.LL_6_INITIATE_DELETE_LISTING:
        nextState = DIALOG_STATE.LL_7_DELETE_LISTING;
        if (!engagementRecord) {
          throw new Error("Engagement record not found.");
        }
        // Get a list of all the listings for this user. Add to partialSearchTable.
        const userListings = await prismaClient.listing.findMany({
          where: {
            platformId: PLATFORM_IDS.TELEGRAM,
            platformUserId: String(this.inputMessage.userIdentifier),
          },
        });
        if (!userListings.length) {
          throw new Error(
            "User was supposed to have listings, but I found None."
          );
        }
        const userListingIds = userListings.map((listing) => listing.id);
        // Create a partial search with the listing ids. Add it to the engagement record
        partialSearchEntry = await prismaClient.partialSearch.create({
          data: {
            results: userListingIds,
          },
        });
        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
            partialSearchId: partialSearchEntry.id,
          },
        });
        responses.push(
          ...userListings.map((listing, index) =>
            this.createListingMessage(listing, index)
          )
        );
        responses.push(
          this.textMessage(this.queryHub.ADD_LISTING_QUERIES.DELETE_WHICH)
        );
        break;

      case DIALOG_STATE.LL_7_DELETE_LISTING:
        nextState = DIALOG_STATE.OPENING;
        if (!engagementRecord || !partialSearchEntry) {
          throw new Error("Engagement record not found.");
        }
        const listingIndex = Number(strippedMessage);
        if (
          isNaN(listingIndex) ||
          listingIndex < 1 ||
          listingIndex > partialSearchEntry.results.length
        ) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        const listingId = partialSearchEntry.results[listingIndex - 1];
        const listingToDelete = await prismaClient.listing.findUnique({
          where: {
            id: listingId,
          },
        });
        if (!listingToDelete) {
          throw new Error("Listing to delete not found.");
        }
        await this.botManager.deleteImages(listingToDelete.imageUrls);
        await prismaClient.listing.delete({
          where: {
            id: listingId,
          },
        });
        this.numberOfListings -= 1;
        // If there are no more listings, remove the contact info of the poster
        if (!this.numberOfListings) {
          await this.botManager.removeUsername(this.inputMessage.message);
        }

        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
          },
        });
        responses.push(
          this.textMessage(
            this.queryHub.ADD_LISTING_NOTICES.POST_DELETE_LISTING_MESSAGE
          )
        );
        break;

      case DIALOG_STATE.T_1_DISTRICT:
        nextState = DIALOG_STATE.T_2_MAX_RENT;
        // Check if the message is a valid district code (one of the numeric values in the ADDIS_DISTRICT_CODES object)
        districtCode = Number(strippedMessage);
        // get the district key whose value is the district code
        district = Object.keys(ADDIS_DISTRICT_CODES).find(
          (key: any) =>
            ADDIS_DISTRICT_CODES[key as keyof typeof ADDIS_DISTRICT_CODES] ===
            districtCode
        );
        if (isNaN(districtCode) || !district) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        // Create a partial search with the district. Add it to the engagement record
        partialSearchEntry = await prismaClient.partialSearch.create({
          data: {
            district,
          },
        });
        if (!partialSearchEntry) {
          throw new Error("Partial search could not be created.");
        }
        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
            partialSearchId: partialSearchEntry.id,
          },
        });
        responses.push(
          this.textMessage(this.queryHub.GET_LISTING_QUERIES.MONTHLY_RENT)
        );
        break;

      case DIALOG_STATE.T_2_MAX_RENT:
        nextState = DIALOG_STATE.OPENING;
        if (!engagementRecord || !engagementRecord.partialSearchId) {
          throw new Error("Engagement record not found.");
        }
        monthlyRent = Number(strippedMessage);
        if (isNaN(monthlyRent)) {
          responses.push(...this.getErrorMessageAndPrompt(this.dialogState));
          break;
        }
        // Ensure that the partial search is non-null
        if (!partialSearchEntry || !partialSearchEntry.district) {
          throw new Error("Partial search is missing required fields.");
        }
        // Convert the rent to a whole number
        monthlyRent = Math.round(monthlyRent);
        // Update the partial search with the monthly rent
        partialSearchEntry = await prismaClient.partialSearch.update({
          where: {
            id: engagementRecord.partialSearchId,
          },
          data: {
            monthlyRent,
          },
        });
        if (!partialSearchEntry || !partialSearchEntry.district) {
          throw new Error("Partial search could not be updated.");
        }
        // Get a list of a max of 10 listings that match the partial search
        const listings = await prismaClient.listing.findMany({
          where: {
            address: {
              contains: partialSearchEntry.district,
            },
            monthlyRent: {
              lte: monthlyRent,
            },
          },
          take: 10,
        });
        // Add the listing ids to results array in the partialSearch table
        const listingIds = listings.map((listing) => listing.id);
        const userContacts = await Promise.all(
          listings.map(async (listing) => {
            return await this.botManager.getUsername(listing.platformUserId);
          })
        );
        const listingsToSend = listings.filter(
          (_listing, index) => userContacts[index]
        );
        const savedContacts = userContacts.filter((contact) => contact);
        if (listingsToSend.length === 0) {
          responses.push(
            this.textMessage(
              this.queryHub.GET_LISTING_NOTICES.NO_RESULTS_MESSAGE
            )
          );
          await this.clearEngagementRecord(engagementRecord);
          break;
        }
        await prismaClient.partialSearch.update({
          where: {
            id: partialSearchEntry.id,
          },
          data: {
            results: {
              set: listingsToSend.map((listing) => listing.id),
            },
          },
        });
        const listingMessages = listingsToSend.map((listing, index) =>
          this.createListingMessage(listing, index, savedContacts[index])
        );
        // For now just return each listing id.
        responses.push(...listingMessages);
        await prismaClient.tgEngagementRecord.update({
          where: {
            chatId: Number(this.inputMessage.engagementIdentifier),
          },
          data: {
            currentState: nextState,
          },
        });
        responses.push(
          this.textMessage(
            this.queryHub.GET_LISTING_NOTICES.POST_RESULTS_MESSAGE
          )
        );
        break;
      // If none of the cases match, set to opening state and return the result
      default:
        this.dialogState = DIALOG_STATE.OPENING;
        return this.getNextMessages();
    }

    // Set the footerMessage
    let footerMessage = "";
    if (nextState !== DIALOG_STATE.START_DIALOG) {
      footerMessage = `${this.startOverSymbol} ${this.queryHub.LABELS.START_OVER}`;
    }
    if (
      this.numberOfListings &&
      nextState !== DIALOG_STATE.LL_7_DELETE_LISTING
    ) {
      footerMessage = `${this.deleteListingSymbol} ${this.queryHub.LABELS.DELETE_LISTING}\n${footerMessage}`;
    }

    // Go through the list of responses starting from the last one and pick out
    // the first message that is of type TEXT. Add the footerMessage to that message
    for (let i = responses.length - 1; i >= 0; i--) {
      if (responses[i].type === MessageType.TEXT) {
        const textPayload = responses[i].payload as PlainText;

        textPayload.text += `\n\n${footerMessage}`;
        break;
      }
    }

    return responses;
  }

  /**
   * Create a generic message for displaying a listing entry
   */
  private createListingMessage(
    listing: Listing,
    index: number,
    contact: string | null = null
  ): GenericMessage {
    let caption = `${index + 1}. `;
    if (contact) {
      caption += `\n${this.queryHub.LABELS.CONTACT}: ${contact}`;
    }
    caption += `\n${this.queryHub.LABELS.MONTHLY_RENT}: ${listing.monthlyRent} ${this.queryHub.LABELS.BIRR}`;
    caption += `\n${this.queryHub.LABELS.DISTRICT}: ${
      this.queryHub.ADDIS_DISTRICTS[
        listing.address as keyof typeof ADDIS_DISTRICTS
      ]
    }`;
    return this.imageMessage(listing.imageUrls, caption);
  }

  /**
   * Get the error message and the appropriate prompt to show again.
   */
  private getErrorMessageAndPrompt(
    currentState: DIALOG_STATE
  ): GenericMessage[] {
    const responses: GenericMessage[] = [];
    responses.push(this.textMessage(this.queryHub.OTHER_FRAGMENTS.ERROR_INPUT));
    responses.push(this.textMessage(this.getCurrentPrompt(currentState)));
    return responses;
  }

  private textMessage(text: string): GenericMessage {
    return { type: MessageType.TEXT, payload: { text } };
  }

  private imageMessage(imageUrls: string[], caption: string): GenericMessage {
    return { type: MessageType.IMAGES, payload: { imageUrls, caption } };
  }

  /**
   * Fetch the appropriate prompt to show the user based on the current state of the dialog
   */
  private getCurrentPrompt(dialogState: DIALOG_STATE): string {
    switch (dialogState) {
      case DIALOG_STATE.START_DIALOG:
        return this.promptForUserType();
      case DIALOG_STATE.LL_1_PICTURES:
        return this.queryHub.ADD_LISTING_QUERIES.PICTURES;
      case DIALOG_STATE.LL_2_MORE_PICTURES:
        return this.queryHub.ADD_LISTING_QUERIES.MORE_PICTURES;
      case DIALOG_STATE.LL_3_NUM_OF_ROOMS:
        return this.queryHub.ADD_LISTING_QUERIES.NUM_OF_ROOMS;
      case DIALOG_STATE.LL_4_DISTRICT:
        return this.districtsMenuWithPrompt(dialogState);
      case DIALOG_STATE.LL_5_MONTHLY_RENT:
        return this.queryHub.ADD_LISTING_QUERIES.MONTHLY_RENT;
      case DIALOG_STATE.LL_7_DELETE_LISTING:
        return this.queryHub.ADD_LISTING_QUERIES.DELETE_WHICH;
      case DIALOG_STATE.T_1_DISTRICT:
        return this.districtsMenuWithPrompt(dialogState);
      case DIALOG_STATE.T_2_MAX_RENT:
        return this.queryHub.GET_LISTING_QUERIES.MONTHLY_RENT;
      default:
        throw new Error("Invalid dialog state.");
    }
  }

  /**
   * Cleans the existing engagement record and deletes the associated
   * partial search or partial listing records. This will make the dialog
   * to start afresh.
   */
  private async clearEngagementRecord(
    engagment: TgEngagementRecord,
    deleteImages: boolean = true
  ): Promise<TgEngagementRecord> {
    if (engagment.partialListingId) {
      // First delete any photos uploaded for the partial listing
      const partialListing = await prismaClient.partialListing.findUnique({
        where: {
          id: engagment.partialListingId,
        },
      });
      if (
        deleteImages &&
        partialListing?.imageUrls &&
        partialListing.imageUrls?.length > 0
      ) {
        await this.botManager.deleteImages(partialListing.imageUrls);
      }

      await prismaClient.partialListing.delete({
        where: {
          id: engagment.partialListingId,
        },
      });
    }
    if (engagment.partialSearchId) {
      await prismaClient.partialSearch.delete({
        where: {
          id: engagment.partialSearchId,
        },
      });
    }
    const newEngagment = await prismaClient.tgEngagementRecord.update({
      where: {
        chatId: Number(this.inputMessage.engagementIdentifier),
      },
      data: {
        currentState: DIALOG_STATE.OPENING,
        partialListingId: null,
        partialSearchId: null,
      },
    });
    return newEngagment;
  }

  private districtsMenuWithPrompt(nextState: DIALOG_STATE): string {
    const prompt =
      nextState === DIALOG_STATE.LL_4_DISTRICT
        ? this.queryHub.ADD_LISTING_QUERIES.DISTRICT
        : this.queryHub.GET_LISTING_QUERIES.DISTRICT;
    const districts = Object.keys(ADDIS_DISTRICT_CODES);
    let districtsMenu = "";
    districts.forEach((district) => {
      districtsMenu += `\n${
        ADDIS_DISTRICT_CODES[district as keyof typeof ADDIS_DISTRICT_CODES]
      } -- ${
        this.queryHub.ADDIS_DISTRICTS[district as keyof typeof ADDIS_DISTRICTS]
      }`;
    });
    return prompt + "\n" + districtsMenu;
  }

  /**
   * Returns the current state of the dialog with the user in this chat,
   * after checking the engagement records.
   * If no entry is found, returns the opening state.
   */
  private static async getDialogState<PlatformMessageType>(
    message: ChatMessageWrapper<PlatformMessageType>
  ): Promise<DIALOG_STATE> {
    if (!message.engagementIdentifier) {
      throw new Error("Telegram chat ID not found.");
    }
    // Get the state from the engagement record
    const engagementRecord = await prismaClient.tgEngagementRecord.findFirst({
      where: {
        chatId: Number(message.engagementIdentifier),
      },
    });
    if (engagementRecord) {
      return engagementRecord.currentState;
    }
    return DIALOG_STATE.OPENING;
  }

  /**
   * Returns the opening statement, and the first query to be sent to the user.
   */
  private introStatement(): string {
    let intro = "";
    intro += this.queryHub.OPENING_QUERIES.OPENING;
    intro += " ";
    intro += this.queryHub.OPENING_QUERIES.INTRO;
    return intro;
  }

  private promptForUserType(): string {
    let prompt = "";
    prompt += this.queryHub.OPENING_QUERIES.USER_TYPE;
    prompt += "\n\n";
    prompt +=
      `${USER_TYPES_ENUM.LANDLORD} ` + this.queryHub.USER_TYPES.LANDLORD;
    prompt += "\n";
    prompt += `${USER_TYPES_ENUM.TENANT} ` + this.queryHub.USER_TYPES.TENANT;
    return prompt;
  }
}
