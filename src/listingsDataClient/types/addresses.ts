import { z } from "zod";

export const internationalAddressSchema = z.object({
  street: z.string(),
  state: z.string(),
  zip: z.string(),
});

export type InternationalAddress = z.infer<typeof internationalAddressSchema>;

export const ethiopianAddressSchema = z.object({
  kebele: z.string(),
  woreda: z.string(),
  zone: z.string(),
  region: z.string(),
});

export type EthiopianAddress = z.infer<typeof ethiopianAddressSchema>;

export const addisAbabaAddressSchema = z.object({
  houseNumber: z.string().optional(),
  kebele: z.string(),
  woreda: z.string(),
  subCity: z.string(),
});

export type AddisAbabaAddress = z.infer<typeof addisAbabaAddressSchema>;

export const addressSchema = z.object({
  city: z.string(),
  country: z.string(),
  address: z.union([
    internationalAddressSchema,
    ethiopianAddressSchema,
    addisAbabaAddressSchema,
    // TODO: For now, allow the address to also be just a string.
    z.string(),
  ]),
});

export type Address = z.infer<typeof addressSchema>;
