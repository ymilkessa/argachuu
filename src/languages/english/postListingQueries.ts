export const ADD_LISTING_QUERIES = {
  PICTURES: "Send pictures of the house via this chat.",
  MORE_PICTURES:
    "Send more pictures.\nOr send '#' if you're done sending pictures.",
  DISTRICT: "In which sub city is this house located?",
  KEBELE: "What kebele is the house located in?",
  MONTHLY_RENT: "What is the monthly rent?",
  // Questions below are on house details.
  WALL_MATERIAL_QUERY: "What material is the wall made of?",
  NUM_OF_ROOMS: "How many rooms does the house have?",
  // TODO: Currently not asking other house details. (Maybe not necessary? i.e. floor material, number of bathrooms)
  DELETE_WHICH: "Enter the number of the listing that you want to delete.",
};

export const ADD_LISTING_NOTICES = {
  STATE_NUM_OF_LISTINGS: "You have posted this many listings: ",
  NO_LISTINGS_TO_DELETE: "You have no listings to delete.",
  POST_ADD_LISTING_MESSAGE:
    "You have successfully posted a listing. You will receive a message when someone is interested in the house.",
  POST_DELETE_LISTING_MESSAGE: "You have successfully deleted a listing.",
  USERNAME_NOT_IDENTIFIED:
    "Your username could not be identified. Please try again.",
};

export const WALL_MATERIAL = {
  CEMENT: "cement",
  MUD: "mud",
  OTHER: "other",
};

export type ADD_LISTING_QUERIES_TYPE = typeof ADD_LISTING_QUERIES;
export type WALL_MATERIALS_TYPE = typeof WALL_MATERIAL;
