import { QueryHub } from "./english/queryHub";

export const oromiffaQueries: QueryHub = {
  ADD_LISTING_QUERIES: {
    PICTURES: "Karaa haasaa kanaa suuraa manichaa ergi.",
    MORE_PICTURES: "Suuraa dabalataa ergi yookan itti fufuuf '#' ergi.",
    DISTRICT: "Manni kun kutaa magaalaa kam keessatti argama?",
    KEBELE: "Manichi ganda kam keessatti argama?",
    MONTHLY_RENT: "Kiraan ji'aa meeqadha?",
    WALL_MATERIAL_QUERY: "Dallaan simintoo moo dhoqqee irraa hojjetame?",
    NUM_OF_ROOMS: "Manichi kutaa meeqa qaba?",
    DELETE_WHICH: "Lakkoofsa tarree haquu barbaaddu galchi.",
  },
  WALL_MATERIAL: {
    CEMENT: "Simintoo",
    MUD: "Dhoqqee",
    OTHER: "Kan biraa",
  },
  GET_LISTING_QUERIES: {
    DISTRICT: "Kutaa magaalaa kam keessaa mana barbaadda?",
    MONTHLY_RENT: "Kiraan ji'aa kaffaluu barbaaddu olaanaan isaa meeqa?",
  },
  ADDIS_DISTRICTS: {
    ADDIS_KETEMA: "Addis Katamaa",
    AKAKI_KALITI: "Aqaaqii",
    ARADA: "Araadaa",
    BOLE: "Boolee",
    GULLELE: "Gulallee",
    KIRKOS: "Qiirqoos",
    KOLFE_KERANIO: "Kolfee qaraaniiyo",
    LIDETA: "Lidataa",
    NIFAS_SILK: "Nifaas Silk",
    YEKA: "Yakkaa",
  },
  OPENING_QUERIES: {
    OPENING: "Akkam jirtu?",
    INTRO:
      "Kuni chaatbootii kireessaa fi kireeffataa wal qunnamsiisuuf gargaarudha.",
    USER_TYPE: "Kireesssaa moo kireeffataadha?",
  },
  USER_TYPES: {
    LANDLORD: "Kireessaa",
    TENANT: "Kireeffataa",
  },
  OTHER_FRAGMENTS: {
    KEBELE_PROMPT: "e.g. 01",
    ERROR_INPUT: "Galtee sirrii galchi.",
  },
  ADD_LISTING_NOTICES: {
    STATE_NUM_OF_LISTINGS: "Tarreeffamoota hammana maxxansiteetta:",
    POST_ADD_LISTING_MESSAGE:
      "Milkaa'inaan tarreefama maxxansiteetta. Namni manicha barbaadu yeroo argamu ergaan si qaqqaba.",
    NO_LISTINGS_TO_DELETE: "Tarreefama haquuf hin qabdu.",
    POST_DELETE_LISTING_MESSAGE: "Tarreefama tokko milkaa'inaan haqxee jirta.",
    USERNAME_NOT_IDENTIFIED:
      "Maqaa fayyadamaa keessan argachuu hin dandeenye. Mee irra deebi'ii yaalaa.",
  },
  GET_LISTING_NOTICES: {
    POST_RESULTS_MESSAGE:
      "Abbootii qabeenyaa karaa maqaa fayyadamtootaa kennameen ergaa erguu dandeessu.",
    NO_RESULTS_MESSAGE: "Bu'aan ulaagaa ati kennite wajjin walsimu hin jiru.",
  },
  LABELS: {
    DELETE_LISTING: "Tarreefama haquu",
    START_OVER: "Irra deebi'ii jalqabi",
    MONTHLY_RENT: "Kiraa ji'aa",
    DISTRICT: "kutaa magaalaa",
    BIRR: "birrii",
    CONTACT: "Kireessaa",
  },
};
