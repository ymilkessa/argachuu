-- AlterTable
ALTER TABLE "PartialListing" ALTER COLUMN "numberOfRooms" DROP NOT NULL,
ALTER COLUMN "monthlyRent" DROP NOT NULL,
ALTER COLUMN "district" DROP NOT NULL;

-- AlterTable
ALTER TABLE "PartialSearch" ALTER COLUMN "price" DROP NOT NULL,
ALTER COLUMN "district" DROP NOT NULL;
