import { ADDIS_DISTRICTS } from "./addressOptions";
import { GET_LISTING_NOTICES, GET_LISTING_QUERIES } from "./getListingQueries";
import {
  ADD_LISTING_NOTICES,
  ADD_LISTING_QUERIES,
  WALL_MATERIAL,
} from "./postListingQueries";

const OTHER_FRAGMENTS = {
  /**
   * e.g 'For example, 01'
   */
  KEBELE_PROMPT: "For example, 01",
  ERROR_INPUT: "Please enter a valid input.",
};

const LABELS = {
  DELETE_LISTING: "Delete a listing",
  START_OVER: "Start over",
  MONTHLY_RENT: "Monthly rent",
  DISTRICT: "District",
  BIRR: "Birr",
  CONTACT: "Contact",
};

const OPENING_QUERIES = {
  OPENING: "Hello! This is TheFellig!",
  INTRO:
    "I can help you find a new place to live, or new tenants for your house.",
  USER_TYPE: "Are you a landlord or a tenant? Send the corresponding number.",
};

const USER_TYPES = {
  LANDLORD: "landlord",
  TENANT: "tenant",
};

export enum USER_TYPES_ENUM {
  UNIDENTIFIED = 0,
  LANDLORD = 1,
  TENANT = 2,
}

export type QueryHub = {
  ADD_LISTING_QUERIES: typeof ADD_LISTING_QUERIES;
  GET_LISTING_QUERIES: typeof GET_LISTING_QUERIES;
  ADDIS_DISTRICTS: typeof ADDIS_DISTRICTS;
  WALL_MATERIAL: typeof WALL_MATERIAL;
  OTHER_FRAGMENTS: typeof OTHER_FRAGMENTS;
  OPENING_QUERIES: typeof OPENING_QUERIES;
  USER_TYPES: typeof USER_TYPES;
  ADD_LISTING_NOTICES: typeof ADD_LISTING_NOTICES;
  GET_LISTING_NOTICES: typeof GET_LISTING_NOTICES;
  LABELS: typeof LABELS;
};

export const englishQueries: QueryHub = {
  ADD_LISTING_QUERIES,
  GET_LISTING_QUERIES,
  ADDIS_DISTRICTS,
  WALL_MATERIAL,
  OTHER_FRAGMENTS,
  OPENING_QUERIES,
  USER_TYPES,
  ADD_LISTING_NOTICES,
  GET_LISTING_NOTICES,
  LABELS,
};
