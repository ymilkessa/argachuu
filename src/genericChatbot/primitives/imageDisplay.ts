export interface ImageDisplay {
  caption: string;
  imageUrls: string[];
}
