export const GET_LISTING_QUERIES = {
  DISTRICT: "In which sub city are you looking for a house?",
  // TODO: Avoid asking for kebele.
  MONTHLY_RENT: "What is the maximum monthly rent that you would like to pay?",
};

export const GET_LISTING_NOTICES = {
  POST_RESULTS_MESSAGE:
    "Feel free to message a landlord using the contact username provided.",
  NO_RESULTS_MESSAGE: "No results found.",
};

export type GET_LISTING_QUERIES_TYPE = typeof GET_LISTING_QUERIES;
