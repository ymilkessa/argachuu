import { Message as ReceivedMessage } from "./receivedTypes/message";

type MediaGroup = {
  mediaGroupId: string;
  chatId: number;
  initialTimestamp: number;
};

// Create a singleton class that houses a map of media groups where the keys are chat ids and the values are
// arrays of MediaGroup objects.
// This class should have a method called 'addNewGroup' that takes in
// a message with a media_group_id. If the chat id is not in the map, then it adds a new key value pair
// where the key is the chat id of the message and the value is a new array with a single MediaGroup object.
// If the chat id exists as a key, but the media group is not in the array, then it creates a new media group
// and appends it to the array. Then it finally returns true.
// If the media group id is already in the map, then it simply returns false.

export class MediaGroupTracker {
  private static instance: MediaGroupTracker;
  private mediaGroups: Map<number, MediaGroup[]>;

  private constructor() {
    this.mediaGroups = new Map();
  }

  public static getInstance(): MediaGroupTracker {
    if (!MediaGroupTracker.instance) {
      MediaGroupTracker.instance = new MediaGroupTracker();
    }
    return MediaGroupTracker.instance;
  }

  public addNewGroup(message: ReceivedMessage): boolean {
    const chatId = message.chat.id;
    const mediaGroupId = message.media_group_id;
    const initialTimestamp = message.date;

    if (!mediaGroupId) {
      return false;
    }

    const relevantMediaGroups = this.mediaGroups.get(chatId);
    if (!relevantMediaGroups) {
      this.mediaGroups.set(chatId, [
        {
          mediaGroupId,
          chatId,
          initialTimestamp,
        },
      ]);
      return true;
    }

    const existingMediaGroup = relevantMediaGroups.find(
      (mediaGroup) => mediaGroup.mediaGroupId === mediaGroupId
    );
    if (existingMediaGroup) {
      return false;
    }

    relevantMediaGroups.push({
      mediaGroupId,
      chatId,
      initialTimestamp,
    });
    return true;
  }

  public removeGroup(message: ReceivedMessage): boolean {
    const chatId = message.chat.id;
    const mediaGroupId = message.media_group_id;

    if (!mediaGroupId) {
      return false;
    }

    const mediaGroups = this.mediaGroups.get(chatId);
    if (!mediaGroups) {
      return false;
    }

    const existingMediaGroup = mediaGroups.find(
      (mediaGroup) => mediaGroup.mediaGroupId === mediaGroupId
    );
    if (!existingMediaGroup) {
      return false;
    }

    const index = mediaGroups.indexOf(existingMediaGroup);
    mediaGroups.splice(index, 1);
    return true;
  }
}
