import express from "express";
import dotenv from "dotenv";

dotenv.config();

import api from "./api";
import { getAllTgBots } from "./platforms/telegram/botManager";

async function main() {
  const app = express();

  // Apply the middle ware for the api
  app.use(api);

  // Get and apply the middle wares for the telegram bots
  const tgBots = await getAllTgBots();
  for (const bot of tgBots) {
    app.use(bot.app);
  }

  const port = process.env.PORT || 3000;
  app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });
}

main();
