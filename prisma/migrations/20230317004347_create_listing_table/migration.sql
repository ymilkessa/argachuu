-- CreateTable
CREATE TABLE "Listing" (
    "id" SERIAL NOT NULL,
    "platformId" INTEGER NOT NULL,
    "platformUserId" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "country" TEXT NOT NULL,
    "address" TEXT NOT NULL,
    "monthlyRent" INTEGER NOT NULL,
    "houseDetails" JSONB NOT NULL,
    "imageUrls" TEXT[],
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Listing_pkey" PRIMARY KEY ("id")
);
