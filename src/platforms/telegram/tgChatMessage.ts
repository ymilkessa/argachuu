import { ChatMessageWrapper } from "../../genericChatbot/chatMessage";
import { Message } from "./receivedTypes/message";

export class TgChatMessageWrapper extends ChatMessageWrapper<Message> {
  constructor(message: Message) {
    super({
      engagementIdentifier: message.chat.id,
      userIdentifier: message.from!.id,
      message,
    });
  }

  getMessageText(): string | undefined {
    return this.message.text;
  }
}
