-- CreateTable
CREATE TABLE "PartialListing" (
    "id" TEXT NOT NULL,
    "platformId" INTEGER NOT NULL,
    "platformUserId" TEXT NOT NULL,
    "imageUrls" TEXT[],
    "numberOfRooms" INTEGER NOT NULL,
    "monthlyRent" INTEGER NOT NULL,
    "district" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "PartialListing_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PartialSearch" (
    "id" TEXT NOT NULL,
    "platformId" INTEGER NOT NULL,
    "platformUserId" TEXT NOT NULL,
    "price" INTEGER NOT NULL,
    "district" TEXT NOT NULL,
    "results" TEXT[],
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "PartialSearch_pkey" PRIMARY KEY ("id")
);
