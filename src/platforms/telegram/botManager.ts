import axios from "axios";
import { v4 } from "uuid";
import express from "express";
import {
  LANGUAGE_QUERY_HUB,
  LANGUAGE_TYPE,
} from "../../languages/allLanguageModes";
import { englishQueries, QueryHub } from "../../languages/english/queryHub";
import { Message as ReceivedMessage } from "./receivedTypes/message";
import { Message as SentMessage } from "./sentTypes/message";
import { File } from "./receivedTypes/file";
import { PhotoSize } from "./receivedTypes/photo";
import { DialogHandler } from "../../genericChatbot/dialogHandler";
import { StorageClient } from "../../dataStorage/storageClient";
import { MediaGroup } from "./sentTypes/mediaGroup";
import { ImageDisplay } from "../../genericChatbot/primitives/imageDisplay";
import { PlainText } from "../../genericChatbot/primitives/plainText";
import { MessageType } from "../../genericChatbot/primitives/genericMessage";
import { ChatMessageWrapper } from "../../genericChatbot/chatMessage";
import { TgChatMessageWrapper } from "./tgChatMessage";
import { MediaGroupTracker } from "./mediaGroupTracker";
import { PrismaClient } from "@prisma/client";

const tgApiRoute = "https://api.telegram.org";

const BOT_TOKENS: Partial<{
  [key in LANGUAGE_TYPE]: string | undefined;
}> = {
  OROMIFFA: process.env.ARGACHUU_BOT_TOKEN,
  AMHARIC: process.env.AFFALIG_BOT_TOKEN,
  ENGLISH: process.env.THEFELLIG_BOT_TOKEN,
};

export const getAllTgBots = async (): Promise<BotManager[]> => {
  const allBots = Object.keys(LANGUAGE_QUERY_HUB)
    .filter((key) => {
      return (
        BOT_TOKENS[key as LANGUAGE_TYPE] &&
        LANGUAGE_QUERY_HUB[key as LANGUAGE_TYPE]
      );
    })
    .map(async (key) => {
      const language = key as LANGUAGE_TYPE;
      const token = BOT_TOKENS[language] as string;
      return await BotManager.startNewBot(token, language);
    });
  return Promise.all(allBots);
};

const prismaClient = new PrismaClient();

export class BotManager {
  private token: string;
  private uri: string;
  private webhookUrl: string;
  private sendMessageEndpoint: string;
  private getFileEndpoint: string;
  private sendMediaGroupEndpoint: string;

  private queryHub: QueryHub;
  private storageClient: StorageClient;
  private mediaGroupTracker: MediaGroupTracker;

  public app: express.Application;

  private constructor(token: string, queryHub: QueryHub = englishQueries) {
    this.token = token;
    this.uri = `/webhook/${v4()}`;
    this.webhookUrl = `${process.env.BASE_URL}${this.uri}`;
    this.sendMessageEndpoint = `${tgApiRoute}/bot${this.token}/sendMessage`;
    this.getFileEndpoint = `${tgApiRoute}/bot${this.token}/getFile`;
    this.sendMediaGroupEndpoint = `${tgApiRoute}/bot${this.token}/sendMediaGroup`;

    this.app = express();
    this.app.use(express.json());
    this.setEndpoints();

    this.queryHub = queryHub;
    this.storageClient = new StorageClient();
    this.mediaGroupTracker = MediaGroupTracker.getInstance();
  }

  static async startNewBot(token: string, language: LANGUAGE_TYPE) {
    const queryHub = LANGUAGE_QUERY_HUB[language];
    if (!queryHub) {
      throw new Error(`The language '${language}' is not supported.`);
    }
    const botManager = new BotManager(token, queryHub);
    await botManager.setWebhook();
    return botManager;
  }

  /**
   * Sets the webhook address with which the bot will receive and respond to messages.
   * This should only be run once.
   */
  private async setWebhook(): Promise<void> {
    try {
      const response = await axios.get(
        `${tgApiRoute}/bot${this.token}/setWebhook?url=${this.webhookUrl}`
      );
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  }

  private setEndpoints(): void {
    this.app.post(this.uri, async (req, res) => {
      if (!req.body.message) {
        return res.send();
      }
      await this.handleMessages(req.body.message);
      res.send();
    });
  }

  private async handleMessages(message: ReceivedMessage): Promise<void> {
    const chatId = message.chat.id;
    const chatMessage = new TgChatMessageWrapper(message);
    try {
      const dialogManager =
        await DialogHandler.getDialogHandler<ReceivedMessage>(
          chatMessage,
          this.queryHub,
          this
        );
      const mediaGroupTrackingResult = this.trackMediaGroups(message);
      const nextMessages = await dialogManager.getNextMessages();

      // If this message is part of an already-tracked media group, then don't send a response.
      if (!mediaGroupTrackingResult.returnResponse) {
        return;
      }

      // Send the responses in the same order as opposed to in parallel.
      for (const nextMessage of nextMessages) {
        if (nextMessage.type === MessageType.TEXT) {
          await this.sendTextMessage(chatId, nextMessage.payload);
        } else if (nextMessage.type === MessageType.IMAGES) {
          await this.sendImages(chatId, nextMessage.payload);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  public async deleteImages(photoUrls: string[]) {
    return await Promise.all(
      photoUrls.map(async (url) => {
        try {
          await this.storageClient.deleteFile(url);
        } catch (error) {
          console.log(error);
          return;
        }
      })
    );
  }

  public async saveUsername(message: any): Promise<boolean> {
    const { from } = message;
    if (!from || !from.username) {
      return false;
    }
    const currentProfile = await prismaClient.tgContactInfo.findUnique({
      where: {
        platformUserId: String(from.id),
      },
    });
    let result: any;
    if (currentProfile) {
      result = await prismaClient.tgContactInfo.update({
        where: {
          platformUserId: String(from.id),
        },
        data: {
          platformUserName: from.username,
        },
      });
    } else {
      result = await prismaClient.tgContactInfo.create({
        data: {
          platformUserId: String(from.id),
          platformUserName: from.username,
        },
      });
    }
    return true;
  }

  public async removeUsername(message: any): Promise<boolean> {
    const { from } = message;
    if (!from || !from.username || from.id) {
      return false;
    }
    await prismaClient.tgContactInfo.delete({
      where: {
        platformUserId: String(from.id),
      },
    });
    return true;
  }

  public async getUsername(userId: string): Promise<string | null> {
    const user = await prismaClient.tgContactInfo.findUnique({
      where: {
        platformUserId: userId,
      },
    });
    if (!user) {
      return null;
    }
    return "@" + user.platformUserName;
  }

  public async fetchAndSavePhotos<PlatformMessageType>(
    messageWrapper: ChatMessageWrapper<PlatformMessageType>
  ): Promise<string[]> {
    const tgMessageWrapper = messageWrapper as TgChatMessageWrapper;
    const message = tgMessageWrapper.message;
    const { photo, from } = message;
    if (!from || !photo || !photo.length) {
      throw new Error("The message does not contain any photo(s)");
    }
    // From the array of PhotoSize objects, pick the one with the largest valule
    // for the photo_size property
    const largestPhoto = photo.reduce((largest, current) => {
      if (!current.file_size || !largest.file_size) {
        return largest;
      }
      if (current.file_size > largest.file_size) {
        return current;
      }
      return largest;
    });

    const arrayOfTheLargestPhoto = [largestPhoto];

    const photoUrls = arrayOfTheLargestPhoto.map(
      async (photoItem: PhotoSize, imageIndex) => {
        const { file_id: fileId } = photoItem;
        const getFileResponse = await axios.get(this.getFileEndpoint, {
          params: { file_id: fileId },
          headers: { "Content-Type": "application/json" },
        });
        const { file_path: filePath } = getFileResponse.data.result as File;
        if (!filePath) {
          throw new Error("The file path is not defined");
        }
        // The download link has the form https://api.telegram.org/file/bot<token>/<file_path>
        const downloadLink = `${tgApiRoute}/file/bot${this.token}/${filePath}`;

        const response = await axios.get(downloadLink, {
          responseType: "arraybuffer",
        });
        const body = response.data;
        if (!body || !response || !response.headers) {
          throw new Error("The response is not valid");
        }
        const fileType =
          response.headers?.["content-type"] || "application/octet-stream";

        const extension = filePath.split(".").pop() as string;

        const newFileName = `tg-${message.chat.id}-${message.message_id}-${imageIndex}.${extension}`;

        // Now upload the file from the stream
        const uploadUrl = await this.storageClient.uploadFromStream(
          body,
          newFileName,
          fileType
        );
        return uploadUrl;
      }
    );
    return Promise.all(photoUrls);
  }

  /**
   * Send pictures and a caption to a telegram chat.
   * @param chatId The id of the chat to send the message to
   * @param imageDisplay: {caption, imageUrls}: The caption and the urls of the images to send
   */
  private async sendImages(
    chatId: number,
    { caption, imageUrls }: ImageDisplay
  ) {
    const photoGroup = imageUrls.map((url: string, index: number) => {
      const httpUrl = url.replace("https", "http");
      const captionToAdd = index === 0 ? caption : undefined;
      return {
        type: "photo" as "photo",
        media: httpUrl,
        caption: captionToAdd,
      };
    });
    const message: MediaGroup = {
      chat_id: chatId,
      media: photoGroup,
    };
    await axios.post(this.sendMediaGroupEndpoint, message);
  }

  /**
   * Send a plaintext message to the chat.
   * @param chatId The id of the chat to send the message to
   * @param plainText: {text}: The text to send
   */
  private async sendTextMessage(chatId: number, { text }: PlainText) {
    const message: SentMessage = {
      chat_id: chatId,
      text,
    };
    await axios.post(this.sendMessageEndpoint, message);
  }

  private trackMediaGroups(message: ReceivedMessage): {
    returnResponse: boolean;
  } {
    if (!message.media_group_id) {
      return { returnResponse: true };
    }
    const returnResponse = this.mediaGroupTracker.addNewGroup(message);
    if (returnResponse) {
      // Create a callback to delete the media group after 10 seconds.
      setTimeout(() => {
        this.mediaGroupTracker.removeGroup(message);
      }, 10000);
    }
    return { returnResponse };
  }
}
