export type File = {
  file_id: string;
  file_unique_id: string;
  /**
   * Size in bytes
   */
  file_size?: number;
  /**
   * File path. Use https://api.telegram.org/file/bot<token>/<file_path> to get the file.
   */
  file_path?: string;
};
