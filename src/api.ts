import express from "express";

import {
  getListingsByUser,
  getListingById,
} from "./listingsDataClient/getListings";

const app = express();

app.get("/", (req, res) => {
  res.json({ data: "Hello, this is the Argachuu API!" });
});

app.get("/listings", (req, res) => {
  const { platformId, platformUserId } = req.query;
  if (!platformId || !platformUserId) {
    res.status(400).send("Missing query parameters");
    return;
  }
  getListingsByUser(Number(platformId), platformUserId as string)
    .then((listings) => {
      res.json(listings);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send("Internal server error");
    });
});

app.get("/listing/:id", (req, res) => {
  const { id } = req.params;
  if (!id) {
    res.status(400).send("Missing id");
    return;
  }
  getListingById(id)
    .then((listing) => {
      res.json(listing);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).send("Internal server error");
    });
});

/**
 * TODO: Add a POST listing endpoint that takes in a listing, performs
 * schema validation, and saves the listing to the database.
 */

export default app;
