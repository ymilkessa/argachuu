import { amharicQueries } from "./amharic";
import { englishQueries, QueryHub } from "./english/queryHub";
import { oromiffaQueries } from "./oromiffa";

export const LANGUAGE = {
  ENGLISH: "english",
  OROMIFFA: "oromiffa",
  AMHARIC: "amharic",
};

export type LANGUAGE_TYPE = keyof typeof LANGUAGE;

export const LANGUAGE_QUERY_HUB: Partial<{
  [key in LANGUAGE_TYPE]: QueryHub;
}> = {
  ENGLISH: englishQueries,
  AMHARIC: amharicQueries,
  OROMIFFA: oromiffaQueries,
};
