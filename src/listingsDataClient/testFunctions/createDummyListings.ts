import { Address } from "../types/addresses";
import { RentListing } from "../types/listings";

export const getImages = (n: number, startNum = 0): string[] => {
  return Array.from(
    { length: n + startNum },
    (_, i) => `https://picsum.photos/id/${i}/200/300`
  );
};

const createDummyAddress = (): Address => {
  return {
    address: { street: "123 Fake St", state: "Fakestate", zip: "12345" },
    city: "Fakeville",
    country: "Fakecountry",
  };
};

export const createDummyListings = (n: number): RentListing[] => {
  return Array.from({ length: n }, (_, i) => ({
    id: i.toString(),
    ownerId: i.toString(),
    location: createDummyAddress(),
    rentPrice: 1000 + i * 100,
    houseDetails: {
      numOfBedrooms: 2,
      numOfBathrooms: 1,
      wallMaterial: "Mud",
      floorMaterial: "Cement",
      numOfRooms: 3,
    },
    imageUrls: getImages(3, i),
  }));
};
