import { QueryHub } from "./english/queryHub";

export const amharicQueries: QueryHub = {
  ADD_LISTING_QUERIES: {
    PICTURES: "የቤቱን ፎቶ በዚ ቻት ላኩ።",
    MORE_PICTURES: "ተጨማሪ ፎቶ ላኩ። \nከጨረሱ '#' ይላኩ።",
    DISTRICT: "ወረዳው የት ነው?",
    KEBELE: "ቀበሌው የት ነው?",
    MONTHLY_RENT: "ወርሃዊ ኪራይ ስንት ነው?",
    WALL_MATERIAL_QUERY: "ግድግዳው ሲሚንቶ ነው ወይስ ጭቃ?",
    NUM_OF_ROOMS: "ቤቱ ስንት ክፍሎች አሉት?",
    DELETE_WHICH: "ለመሰረዝ የፈለጉትን ቤት ቁጥር ያስገቡ።",
  },
  WALL_MATERIAL: {
    CEMENT: "ሲሚንቶ",
    MUD: "ጭቃ",
    OTHER: "ሌላ",
  },
  GET_LISTING_QUERIES: {
    DISTRICT: "በየትኛው ወረዳ ቤት ይፈልጋሉ?",
    MONTHLY_RENT: "ወርሃዊ ኪራይ ከስንት በታች እንዲሆን ይፈልጋሉ?",
  },
  ADDIS_DISTRICTS: {
    ADDIS_KETEMA: "አዲስ ከተማ",
    AKAKI_KALITI: "አቃቂ ቃሊቲ",
    ARADA: "አራዳ",
    BOLE: "ቦሌ",
    GULLELE: "ጉለሌ",
    KIRKOS: "ቂርቆስ",
    KOLFE_KERANIO: "ኮልፌ ቀራኒዮ",
    LIDETA: "ሊደታ",
    NIFAS_SILK: "ንፋስ ስልክ",
    YEKA: "የካ",
  },
  OPENING_QUERIES: {
    OPENING: "ሰላም!",
    INTRO: "ይህ ነጻ የሆነ የኪራይ ቤት መፈለጊአ አገልግሎት ነው።",
    USER_TYPE: "አክራይ ኖት ወይስ ተክራይ? \nአክራይ ከሆኑ 1ን ይላኩ። ተክራይ ከሆኑ 2ን ይላኩ።",
  },
  USER_TYPES: {
    LANDLORD: "አክራይ",
    TENANT: "ተክራይ",
  },
  OTHER_FRAGMENTS: {
    KEBELE_PROMPT: "ለምሳሌ፡ 01",
    ERROR_INPUT: "እባክዎትን የተጠየቁትን መልክት አይነት ያስገቡ።",
  },
  ADD_LISTING_NOTICES: {
    STATE_NUM_OF_LISTINGS: "እስካሁን እርስዎ የመዘገቡት ቤቶች ቁጥር፡ ",
    POST_ADD_LISTING_MESSAGE: "ቤትዎን አስመስግበዋል! ቤቱን የፈለገ ተከራይ ሲኖር መልክት ይደርሳቹሃል።",
    NO_LISTINGS_TO_DELETE: "በእርስዎ የተመዘገቡ ቤቶች የሉም።",
    POST_DELETE_LISTING_MESSAGE: "ቤቱ ከመዝገብዎ ላይ ተሰርዟል።",
    USERNAME_NOT_IDENTIFIED: "የእርስዎን username መመዝገብ አልተቻለም። እባክዎን እንደገና ይሞክሩ።",
  },
  GET_LISTING_NOTICES: {
    POST_RESULTS_MESSAGE: "የፈለጉትን አከራይ ለማግኘት የተሰጠውን አድራሻ (@ስም) መጠቀም ይችላሉ።",
    NO_RESULTS_MESSAGE: "ያቀረቡትን መስፈርቶች የሚያሙዋላ ቤት የለም።",
  },
  LABELS: {
    DELETE_LISTING: "ቆይ ቤት ልሰርዝ",
    START_OVER: "ቆይ እንደገና ልጀምር",
    MONTHLY_RENT: "ወራዊ ኪራይ",
    DISTRICT: "ወረዳ",
    BIRR: "ብር",
    CONTACT: "ኣከራይ/ተጠያቂ",
  },
};
