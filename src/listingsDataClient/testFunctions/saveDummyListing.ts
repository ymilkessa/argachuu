import { PrismaClient } from "@prisma/client";
import { createDummyListings } from "./createDummyListings";

const prisma = new PrismaClient();

// convert this into a listing object that matches the prisma schema
export const saveDummyListings = async (n: number) => {
  const dummyListings = createDummyListings(n);

  const dbListings = await Promise.all(
    dummyListings.map((listing) => {
      return prisma.listing.create({
        data: {
          platformId: 0,
          platformUserId: listing.ownerId,
          monthlyRent: listing.rentPrice,
          city: listing.location.city,
          country: listing.location.country,
          address: JSON.stringify(listing.location.address),
          houseDetails: {
            numOfRooms: listing.houseDetails.numOfRooms,
            numOfBedrooms: listing.houseDetails.numOfBedrooms,
            numOfBathrooms: listing.houseDetails.numOfBathrooms,
            wallMaterial: listing.houseDetails.wallMaterial,
            floorMaterial: listing.houseDetails.floorMaterial,
          },
          imageUrls: listing.imageUrls,
        },
      });
    })
  );
};
