import { InputMediaPhoto } from "./inputMedia";

export type MediaGroup = {
  chat_id: string | number;
  media: InputMediaPhoto[];
};
