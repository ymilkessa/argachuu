import { Chat } from "./chat";
import { PhotoSize } from "./photo";
import { User } from "./user";
import { Voice } from "./voice";

export interface Message {
  message_id: number;
  message_thread_id?: number;
  from?: User;
  sender_chat?: Chat;
  date: number;
  chat: Chat;
  // Skipping a bunch of fields here
  reply_to_message?: Message;
  edit_date?: number;
  media_group_id?: string;
  // The meat of the message for text messages
  text?: string;
  entities?: MessageEntity[];
  photo?: PhotoSize[];
  voice?: Voice;
  location?: Location;
}

export type MessageEntity = {
  type:
    | "mention"
    | "hashtag"
    | "cashtag"
    | "bot_command"
    | "url"
    | "email"
    | "phone_number"
    | "bold"
    | "italic"
    | "underline"
    | "strikethrough"
    | "spoiler"
    | "code"
    | "pre"
    | "text_link"
    | "text_mention"
    | "custom_emoji";
  offset: number;
  length: number;
  url?: string;
};
