/**
 * User data from Telegram API, according to https://core.telegram.org/bots/api#user
 */
export type User = {
  /**
   * But the id is a 64 bit number in the api
   */
  id: number;
  is_bot: boolean;
  first_name: string;
  last_name?: string;
  username?: string;
  // Probably won't need the stuff below
  language_code?: string;
  is_premium?: boolean;
  added_to_attachment_menu?: boolean;
  can_join_groups?: boolean;
  can_read_all_group_messages?: boolean;
  supports_inline_queries?: boolean;
};
