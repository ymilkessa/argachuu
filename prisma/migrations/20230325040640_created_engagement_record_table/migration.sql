/*
  Warnings:

  - You are about to drop the column `chatId` on the `PartialListing` table. All the data in the column will be lost.
  - You are about to drop the column `createdAt` on the `PartialListing` table. All the data in the column will be lost.
  - You are about to drop the column `currentState` on the `PartialListing` table. All the data in the column will be lost.
  - You are about to drop the column `platformId` on the `PartialListing` table. All the data in the column will be lost.
  - You are about to drop the column `platformUserId` on the `PartialListing` table. All the data in the column will be lost.
  - You are about to drop the column `chatId` on the `PartialSearch` table. All the data in the column will be lost.
  - You are about to drop the column `createdAt` on the `PartialSearch` table. All the data in the column will be lost.
  - You are about to drop the column `currentState` on the `PartialSearch` table. All the data in the column will be lost.
  - You are about to drop the column `platformId` on the `PartialSearch` table. All the data in the column will be lost.
  - You are about to drop the column `platformUserId` on the `PartialSearch` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX "PartialListing_chatId_key";

-- DropIndex
DROP INDEX "PartialSearch_chatId_key";

-- AlterTable
ALTER TABLE "PartialListing" DROP COLUMN "chatId",
DROP COLUMN "createdAt",
DROP COLUMN "currentState",
DROP COLUMN "platformId",
DROP COLUMN "platformUserId";

-- AlterTable
ALTER TABLE "PartialSearch" DROP COLUMN "chatId",
DROP COLUMN "createdAt",
DROP COLUMN "currentState",
DROP COLUMN "platformId",
DROP COLUMN "platformUserId";

-- CreateTable
CREATE TABLE "TgEngagementRecord" (
    "id" TEXT NOT NULL,
    "platformUserId" TEXT NOT NULL,
    "chatId" BIGINT NOT NULL,
    "lastEngagementType" INTEGER NOT NULL,
    "currentState" INTEGER NOT NULL,
    "partialListingId" TEXT,
    "partialSearchId" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "TgEngagementRecord_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "TgEngagementRecord_chatId_key" ON "TgEngagementRecord"("chatId");

-- CreateIndex
CREATE UNIQUE INDEX "TgEngagementRecord_partialListingId_key" ON "TgEngagementRecord"("partialListingId");

-- CreateIndex
CREATE UNIQUE INDEX "TgEngagementRecord_partialSearchId_key" ON "TgEngagementRecord"("partialSearchId");

-- AddForeignKey
ALTER TABLE "TgEngagementRecord" ADD CONSTRAINT "TgEngagementRecord_partialListingId_fkey" FOREIGN KEY ("partialListingId") REFERENCES "PartialListing"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TgEngagementRecord" ADD CONSTRAINT "TgEngagementRecord_partialSearchId_fkey" FOREIGN KEY ("partialSearchId") REFERENCES "PartialSearch"("id") ON DELETE SET NULL ON UPDATE CASCADE;
