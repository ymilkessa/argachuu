import {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3";
import fs from "fs";
import { join } from "path";

export class StorageClient {
  private s3Client: S3Client | null = null;
  private bucketName: string = "";
  private static instance: StorageClient | null = null;

  constructor() {
    if (!StorageClient.instance) {
      // Raise an error if the environmental variables are not set
      if (
        !process.env.AWS_ACCESS_KEY_ID ||
        !process.env.AWS_SECRET_ACCESS_KEY ||
        !process.env.AWS_S3_BUCKET_NAME ||
        !process.env.AWS_REGION
      ) {
        throw new Error(
          "AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY must be set"
        );
      }
      this.s3Client = new S3Client({
        region: process.env.AWS_REGION,
        credentials: {
          accessKeyId: process.env.AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        },
      });
      this.bucketName = process.env.AWS_S3_BUCKET_NAME;
      StorageClient.instance = this;
    }
    return StorageClient.instance;
  }

  /**
   * Uploads a single file to the s3 bucket and returns a promise of the file's url.
   * @param fileName - The name of the file
   * @param fileDir - The path of the file's directory
   * @param contentType - The file type (e.g. "image/jpeg")
   * @returns A promise of the file's url
   * @throws An error if the file is not found or if the upload fails
   * @example
   * const fileUrl = await storageClient.uploadSingleFile("file1", "path/to/file1", "image/jpeg");
   */
  public async uploadSingleFile(
    fileName: string,
    fileDir: string,
    contentType: string
  ): Promise<string> {
    const filePath = join(fileDir, fileName);
    const readStream = fs.createReadStream(filePath);

    const uploadParams = {
      Bucket: this.bucketName,
      Key: fileName,
      Body: readStream,
      ContentType: contentType,
    };
    const uploadCommand = new PutObjectCommand(uploadParams);
    try {
      const result = await this.s3Client!.send(uploadCommand);
      return `https://${this.bucketName}.s3.amazonaws.com/${fileName}`;
    } catch (err) {
      throw new Error("Upload failed");
    }
  }

  /**
   * Uploads multiple files to the s3 bucket and returns a promise of an array of the files' urls.
   * @param fileNames - An array of file names
   * @param fileDir - The path to the directory where the files reside
   * @param contentType - The file type (e.g. "image/jpeg")
   * @returns A promise of an array of the files' urls
   * @throws An error if any of the files are not found or if the upload fails
   * @example
   * const fileUrls = await storageClient.uploadMultipleFiles(["file1", "file2"], "path/to/files", "image/jpeg");
   */
  public async uploadMultipleFiles(
    fileNames: string[],
    fileDir: string,
    contentType: string
  ): Promise<string[]> {
    const fileUrls: string[] = [];
    for (const fileName of fileNames) {
      const fileUrl = await this.uploadSingleFile(
        fileName,
        fileDir,
        contentType
      );
      fileUrls.push(fileUrl);
    }
    return fileUrls;
  }


  /**
   * Deletes a file from the s3 bucket.
   * @param s3Url - The url of the file in the s3 bucket
   * @returns A promise of the deleted file's url
   * @throws An error if the file is not found or if the deletion fails
   * @example
   * const deletedFileUrl = await storageClient.deleteFile("https://bucket.s3.amazonaws.com/file1");
   * // deletedFileUrl = "https://bucket.s3.amazonaws.com/file1"
   */
  public async deleteFile(s3Url: string): Promise<void> {
    const key = s3Url.split("/")[3];
    const deleteParams = {
      Bucket: this.bucketName,
      Key: key,
    };
    try {
      await this.s3Client!.send(new DeleteObjectCommand(deleteParams));
    } catch (err) {
      throw new Error("Deletion failed");
    }
  }

  /**
   * Uploads a file from a stream to the s3 bucket and returns the file's url.
   */
  public async uploadFromStream(
    fileStream: any,
    fileName: string,
    contentType: string
  ): Promise<string> {
    const uploadParams = {
      Bucket: this.bucketName,
      Key: fileName,
      Body: fileStream,
      ContentType: contentType,
    };
    const uploadCommand = new PutObjectCommand(uploadParams);
    try {
      const result = await this.s3Client!.send(uploadCommand);
      return `https://${this.bucketName}.s3.amazonaws.com/${fileName}`;
    } catch (err) {
      throw new Error("Upload failed");
    }
  }
}
