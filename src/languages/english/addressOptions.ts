export const ADDIS_DISTRICTS = {
  ADDIS_KETEMA: "Addis Ketema",
  AKAKI_KALITI: "Akaki Kaliti",
  ARADA: "Arada",
  BOLE: "Bole",
  GULLELE: "Gullele",
  KIRKOS: "Kirkos",
  KOLFE_KERANIO: "Kolfe Keranio",
  LIDETA: "Lideta",
  NIFAS_SILK: "Nifas Silk",
  YEKA: "Yeka",
};

export const ADDIS_DISTRICT_CODES = {
  ADDIS_KETEMA: 1,
  AKAKI_KALITI: 2,
  ARADA: 3,
  BOLE: 4,
  GULLELE: 5,
  KIRKOS: 6,
  KOLFE_KERANIO: 7,
  LIDETA: 8,
  NIFAS_SILK: 9,
  YEKA: 10,
};

export type ADDIS_DISTRICTS_TYPE = typeof ADDIS_DISTRICTS;
