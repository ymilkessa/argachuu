import { PrismaClient, Listing } from "@prisma/client";

const prisma = new PrismaClient();

export const getListingsFromDb = async (): Promise<Listing[]> => {
  const listings = await prisma.listing.findMany();
  return listings;
};

export const getListingsByUser = async (
  platformId: number,
  platformUserId: string
): Promise<Listing[]> => {
  const listings = await prisma.listing.findMany({
    where: {
      platformId,
      platformUserId,
    },
  });
  if (!listings) {
    throw new Error("Listings not found");
  }
  return listings;
};

export const getListingById = async (id: string): Promise<Listing> => {
  const listing = await prisma.listing.findUnique({
    where: {
      id,
    },
  });
  if (!listing) {
    throw new Error("Listing not found");
  }
  return listing;
};
