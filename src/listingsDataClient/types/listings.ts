import { z } from "zod";
import { addressSchema } from "./addresses";

export const houseDetails = z.object({
  numOfRooms: z.number(),
  numOfBedrooms: z.number().optional(),
  numOfBathrooms: z.number().optional(),
  wallMaterial: z.string().optional(),
  floorMaterial: z.string().optional(),
});

export const rentListingSchema = z.object({
  id: z.string(),
  ownerId: z.string(),
  location: addressSchema,
  rentPrice: z.number(),
  houseDetails: houseDetails,
  imageUrls: z.array(z.string()),
});

export type HouseDetails = z.infer<typeof houseDetails>;
export type RentListing = z.infer<typeof rentListingSchema>;
