interface InputMedia {
  type: "photo" | "video" | "animation" | "audio" | "document";
  media: string;
  caption?: string;
  parse_mode?: string;
}

// InputMediaPhoto extends InputMedia. Sest the type to always be "photo"
export interface InputMediaPhoto extends InputMedia {
  type: "photo";
}
